# stm32_unict_lib_2

<i>stm32_unict_lib_2</i> è un <b>fork</b> della libreria <i>stm32_unict_lib</i> usata dagli studenti del corso di Laboratorio di Sistemi a Microcontrollore. <br>
L'idea di base è rendere la libreria <i>stm32_unict_lib</i> ancora più semplice da utilizzare, aggiungendo inoltre le funzionalità mancanti.


## Download
Apri un terminale nella cartella dove vuoi scaricare la libreria ed esegui:

`git clone https://gitlab.com/Datalux/stm32_unict_lib_2`

## Updating
Apri un terminale nella cartella della libreria ed esegui:

`git pull`

## Licenza
GPL v2
